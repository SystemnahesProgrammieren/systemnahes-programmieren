#include "../includes/Automat.h"
#include "../../Scanner/includes/Token.h"

#include <iostream>
using namespace std;

Automat::Automat() {
	currentState = START;
	lastValidState = START;
	actualLine = 1;
	actualColumn = 0;
	charsBack = 0;
	counter = 0;
}

Automat::~Automat() {
}

const char signs[] = { '+', '-', ':', '*', '<', '>', '=', ':', '!', '&', ';', '(',
		')', '{', '}', '[', ']' };


bool Automat::isLetter(char c) {
	if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
		return true;
	} else {
		return false;
	}
}

bool Automat::isSign(char c) {
	for (int i = 0; signs[i] != '\0'; i++) {
		if (c == signs[i]) {
			return true;
		}
	}
	return false;
}

bool Automat::isDigit(char c) {
	if (c >= '1' && c <= '9') {
		return true;
	} else {
		return false;
	}
}

bool Automat::isSpace(char c) {
	if (c == ' ' || c == '\t') {
		return true;
	} else {
		return false;
	}
}

bool Automat::isNewLine(char c) {
	if (c == '\n') {
		return true;
	} else {
		return false;
	}
}

bool Automat::put(char c) {

	actualColumn++;

	switch (currentState) {
	case START: {
		if ((Automat::isLetter(c))) {
			setState(IDENTIFIER);
			return true;
		} else if (Automat::isDigit(c)) {
			setState(INTEGER);
			return true;
		} else if (Automat::isNewLine(c)) {
			actualLine++;
			actualColumn = 1;
			setState(NEWLINE);
			return true;
		} else if (Automat::isSign(c) && c != ':' && c != '<') {
			setState(SIGN);
			return true;
		} else if (c == ':') {
			setState(SIGNA);
			return true;
		} else if (c == '<') {
			setState(SIGNB);
			return true;
		} else if (Automat::isSpace(c)) {
			setState(SPACE);
			return true;
		} else if (c == '/') {
			setState(COMMENTA);
			return true;
		} else {
			setState(ERROR);
			return false;
		}
	}

	case SPACE: {
		if (Automat::isSpace(c)) {
			setState(SPACE);
			return true;
		} else {
			setState(INVALID);
			return false;
		}
	}

	case NEWLINE: {
		if (Automat::isNewLine(c)) {
			actualColumn = 1;
			actualLine++;
			setState(NEWLINE);
			return true;
		} else {
			setState(INVALID);
			return false;
		}
	}

	case SIGN: {
		setState(INVALID);
		return false;
	}

	case SIGNA: {
		//TODO improve and add back counter
		if (c == '=') {
			setState(SIGNC);
			return true;
		} else if (c == '/') { //Comment BEGIN
			setState(COMMENTA);
			return true;
		} else {
			setState(INVALID);
			return false;
		}
	}

	case SIGNB: {
		if (c == ':') {
			setState(SIGNC);
			return true;
		} else {
			setState(INVALID);
			return false;
		}
	}

	case SIGNC: {
		if (c == '>') {
			setState(SIGNC);
			return true;
		} else {
			setState(INVALID);
			return false;
		}
	}

	case SIGND: {
		setState(INVALID);
		return false;
	}

	case INTEGER: {
		if (Automat::isDigit(c)) {
			setState(INTEGER);

			return true;
		} else {
			setState(INVALID);
			return false;
		}
	}

	case IDENTIFIER: {
		if (Automat::isLetter(c) || Automat::isDigit(c)) {
			setState(IDENTIFIER);
			return true;
		} else {
			setState(INVALID);
			return false;
		}
	}

	case COMMENTA: {
		if (c == '*') {
			setState(COMMENTC);
			return true;
		} else if (Automat::isNewLine(c)) {
			//TODO NewLine between Comment Sign valid?
			actualLine++;
			actualColumn = 1;
			setState(COMMENTA);
			return true;
		} else {
			setState(COMMENTA);
			return true;
		}
	}

		//In Comment
	case COMMENTC: { //COMMENT END
		if (c == '*') {
			setState(COMMENTD);
			return true;
		} else if (Automat::isNewLine(c)) {
			actualLine++;
			actualColumn = 1;
			setState(COMMENTC);
			return true;

		} else {
			setState(COMMENTC);
			return true;
		}
	}

	case COMMENTD: {
		if (c == '/') {
			setState(COMMENTD);
			return true;
		} else {
			setState(INVALID);
			return false;
		}

	}

	default: {
		cout << "Default";
		return false;
	}
	}
}

Token::Type Automat::getTypeFromLastValidState() {
	switch (lastValidState) {
	case INTEGER: {
		return Token::INTEGER;
	}

	case NEWLINE: {
		return Token::NEWLINE;
	}

	case SPACE: {
		return Token::SPACE;
	}

	case IDENTIFIER: {
		return Token::IDENTIFIER;
	}

	case SIGN: {
		return Token::SIGN;
	}

	case SIGNA: {
		return Token::SIGN;
		break;
	}

	case SIGNB: {
		return Token::SIGN;
		break;
	}

	case SIGNC: {
		//TODO improve!
		return Token::SIGN_COLONQUAL;
		break;
	}

	case SIGND: {
		return Token::SIGN_BRACKET;
		break;
	}

	case COMMENTA: {
		return Token::COMMENT;
		break;
	}

	case COMMENTB: {
		return Token::COMMENT;
		break;
	}

	case COMMENTC: {
		return Token::COMMENT;
		break;
	}

	case COMMENTD: {
		return Token::COMMENT;
		break;
	}

	case INVALID: {
		return Token::INVALID;
		break;
	}

	case ERROR: {
			return Token::ERROR;
			break;
		}
	}
}

int Automat::getLine() {
	return actualLine;
}

int Automat::getColumn() {
	return actualColumn;
}

void Automat::setState(STATE state) {
	if (state == INVALID) {
		charsBack++;
		currentState = INVALID;
	} else {
		charsBack = 0;
		lastValidState = state;
		currentState = lastValidState;
	}
}

int Automat::getCharsBack() {
	return charsBack;
}

void Automat::getBack(int back) {
	actualColumn -= back;
	charsBack = 0;
	currentState = START;
}

