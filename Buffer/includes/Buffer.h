/*
 * Buffer.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#ifndef BUFFER_H_
#define BUFFER_H_

#include "../../Scanner/includes/Token.h"

class Buffer {
public:
	Buffer();
	virtual ~Buffer();
	char getChar();
	void ungetchar(int back);

private:
	int counter; //only for testing
};

#endif /* BUFFER_H_ */
