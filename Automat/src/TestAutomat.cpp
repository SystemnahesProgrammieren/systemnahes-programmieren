#include "../includes/Automat.h"
#include <iostream>
using namespace std;

void test_if(){
	Automat* automat;
	automat = new Automat();
	cout<<automat->put('i');
	cout<<automat->put('f');
	automat->getBack(0);
	cout<<automat->put('I');
	cout<<automat->put('F');
}

void test_while(){
	Automat* automat;
	automat = new Automat();

	automat->put('w');
	automat->put('h');
	automat->put('i');
	automat->put('l');
	automat->getBack(0);
	automat->put('W');
	automat->put('H');
	automat->put('I');
	automat->put('L');
	automat->put('E');
}

void test_Integer(){
	Automat* automat;
	automat = new Automat();

	automat->put('1');
	automat->getBack(0);	automat->put('1');
	automat->put('2');
	automat->getBack(0);	automat->put('1');
	automat->put('x');
}

void test_falsyInputs(){
	Automat* automat;
	automat = new Automat();

	automat->getBack(0);	automat->put('a');
	automat->put('1');
	automat->getBack(0);	automat->put('!');
}

void test_space(){
	Automat* automat;
	automat = new Automat();

	automat->put('w');
	automat->put('i');
	automat->put(' ');
	automat->put('f');
	automat->getBack(0);	automat->put(' ');
	automat->put(' ');
	automat->put('i');
	automat->put(' ');
	automat->put('E');
	automat->getBack(0);	automat->put('i');
	automat->put('f');
	automat->put('i');
	automat->put('f');
}

void test_Signs(){
	Automat* automat;
	automat = new Automat();

	automat->put(':');
	automat->getBack(0);	automat->put('<');
	automat->getBack(0);	automat->put(':');
	automat->put('=');
	automat->getBack(0);	automat->put('<');
	automat->put(':');
	automat->put('>');
	automat->getBack(0);	automat->put('<');
	automat->put(':');
	automat->getBack(0);	automat->put(':');
	automat->put(' ');
	automat->getBack(0);	automat->put('!');
	automat->getBack(0);	automat->put('<');
	automat->put(' ');
	automat->put('>');
	automat->getBack(0);	automat->put('!');
	automat->put(' ');
	automat->put('a');
}

void test_comments(){
	Automat* automat;
	automat = new Automat();

	automat->put(':');
	automat->put('*');
	automat->put('*');
	automat->put(':');
	automat->getBack(0);	automat->put(':');
	automat->put('*');
	automat->put('A');
	automat->put('!');
	automat->put('1');
	automat->put(' ');
	automat->put(':');
	automat->put('*');
	automat->put(':');
	automat->getBack(0);	automat->put(':');
	automat->put('*');
	automat->put(' ');
	automat->put('\t');
	automat->put('\n');
	automat->put(' ');
	automat->put(':');
	automat->put('*');
	automat->put(':');
	automat->put(':');
}

int main (int argc, char* argv[]){
	test_Signs();

}
