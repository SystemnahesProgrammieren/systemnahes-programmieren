/*
 * Information.h
 *
 *  Created on: 04.01.2016
 *      Author: maximilian
 */

#ifndef INFORMATION_H_
#define INFORMATION_H_

class Information {
	public:
		Information(char *lexem);
		const char* getLexem() const;
		~Information();
	private:
		char* lexem;
};

#endif /* INFORMATION_H_ */
