/*
 * Scanner.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#ifndef SCANNER_H_
#define SCANNER_H_

#include "../../Buffer/includes/Buffer.h"
#include "../../Automat/includes/Automat.h"
#include "../../Scanner/includes/Token.h"
#include "../../Scanner/includes/Information.h"


class Scanner {
public:
	Scanner();
	virtual ~Scanner();
	Token* nextToken();
	Token* buildToken(int line, int column, Token::Type type, Information* information, long int value);
	Information* buildInformation(char* lexem);

private:
	Buffer* buffer;
	Automat* automat;
	void reset(int badChars);
	int counter;


};

#endif /* SCANNER_H_ */
