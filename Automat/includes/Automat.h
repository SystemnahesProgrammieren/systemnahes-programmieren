#ifndef Automat_H_
#define Automat_H_

#include "../../Scanner/includes/Token.h"

class Automat {
public:

	typedef enum STATE {

		START,
		INTEGER,
		SPACE,
		SIGN,
		SIGNA,
		SIGNB,
		SIGNC,
		SIGND,
		COMMENTA,
		COMMENTB,
		COMMENTC,
		COMMENTD,
		NEWLINE,
		IDENTIFIER,
		INVALID,
		COMMENT_START,
		COMMENT,
		COMMENT_END,
		ERROR,
		UNDEFINIED
	} STATE;

	inline const char* ToString(STATE state){
		switch (state) {
		case START:return"START";
		case INTEGER:return"INTEGER";
		case SPACE:return"SPACE";
		case SIGN:return"SIGN";
		case SIGNA:return"SIGNA";
		case SIGNB:return"SIGNB";
		case SIGNC:return"SIGNC";
		case SIGND:return"SIGND";

		case COMMENTA:return"COMMENTA";
		case COMMENTB:return"COMMENTB";
		case COMMENTC:return"COMMENTC";
		case COMMENTD:return"COMMENTD";

		case NEWLINE:return"NEWLINE";
		case IDENTIFIER:return"IDENTIFIER";
		case INVALID:return"INVALID";

		case COMMENT_START:return"COMMENT_START";
		case COMMENT:return"COMMENT";
		case COMMENT_END:return"COMMENT_END";

		case ERROR:return"ERROR";
		case UNDEFINIED:return"UNDEFINIED";




		default: return "[Unknown State]";
		}
	};

	STATE currentState;
	STATE lastValidState;
	int actualLine;
	int actualColumn;
	int charsBack;
	int counter;

public:
	Automat();
	virtual ~Automat();
	bool put(char c);
	void getBack(int back);
	void setState(STATE state);
	bool isLetter(char c);
	bool isDigit(char c);
	bool isSpace(char c);
	bool isSign(char c);
	bool isNewLine(char c);

	Token::Type getTypeFromLastValidState();
	int getLine();
	int getColumn();
	int getCharsBack();
};

#endif /* Automat_H_ */
