#ifndef TOKEN_H_
#define TOKEN_H_

#include "../../Scanner/includes/Information.h"

class Token {

public:

	enum Type {
			INTEGER,
			IDENTIFIER,
			SIGN,
			COMMENT,
			SPACE,
			NEWLINE,
			SIGN_BRACKET,
			SIGN_COLONQUAL,
			INVALID,
			ERROR
			//TODO implement other Types
		};

	 inline const char* ToString(Type type)
	 {
	     switch (type)
	     {
			 case INTEGER:return"Integer";
			 case IDENTIFIER:return"Identifier";
			 case SIGN:return"Sign";
			 case COMMENT:return"Comment";
			 case SPACE:return"Space";
			 case NEWLINE:return"Newline";
			 case SIGN_BRACKET:return"Sign_Bracket";
			 case SIGN_COLONQUAL:return"Sign_Colonqual";
			 case INVALID:return"Invalid";
			 case ERROR:return"Error";
			 default: return "[Unknown Type]";
	     }
	 };

	Token(Type token, int line,int column, Information* information,long int value);
	~Token();
	int getLine() const;
	int getColumn() const;
	Type getType() const;
	long int getValue() const;
	Information* getInformation() const;

private:
	int line;
	int column;
	Type type;
	Information* information;
	long int value;
};


#endif /* TOKEN_H_ */
