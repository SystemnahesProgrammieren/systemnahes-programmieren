#include "../includes/Scanner.h"
#include <iostream>
using namespace std;

void printValue(Token* t) {
	cout << "Value: " << t->getValue() << '\n';
}

void printError(Token* t) {
	cout << t->getInformation()->getLexem() << " ist nicht erlaubt\n";
}

void printLexem(Token* t) {
	cout << "Lexem: " << t->getInformation()->getLexem() << '\n';
}

int main(int argc, char **argv) {

	Scanner* scanner;
	scanner = new Scanner();
	Token* t;
	for (int i = 0; i <= 20; i++) {
		t = scanner->nextToken();

		cout << t->ToString(t->getType()) << " ";

		//TODO check memory violation
		cout << "Line: " << t->getLine() << " ";
		cout << "Column: " << t->getColumn() << " ";

		if (t->getType() == Token::INTEGER) {
			printValue(t);
		} else if(t->getType() == Token::ERROR){
			printError(t);
		}
		else {
			printLexem(t);
		}
	}
}


