#include "../includes/Token.h"


Token::Token(Token::Type token,int line,int column,Information* information,long int value){
	this->line = line;
	this->column = column;
	this->type = token;
	this->information = information;
	this->value = value;
}

int Token::getLine() const{
	return line;
}

int Token::getColumn() const{
	return column;
}

Token::Type Token::getType() const{
	return type;
}

long int Token::getValue() const{
	return value;
}

Information* Token::getInformation() const{
	return information;
}
