#include "../includes/Scanner.h"
#include "../../Scanner/includes/Token.h"
#include "../../Scanner/includes/Information.h"
#include <stdlib.h>
#include <errno.h>
#include <iostream>

using namespace std;

Scanner::Scanner() {
	automat = new Automat();
	buffer = new Buffer();
	counter = 0;
}

Scanner::~Scanner() {
	delete automat;
	delete buffer;
}

Token* Scanner::nextToken() {
	char c;
	Token* token;
	Token::Type type;

	const int BUFFER_SIZE = 1024; //TODO maybe get value out of buffer
	char scanner_buffer[BUFFER_SIZE];

	do {
		c = buffer->getChar();
		//cout<<c;
		if(c == -1) break;
		//TODO Handle Errors
		scanner_buffer[counter] = c;
		counter++;

	} while (automat->put(c));

	type = automat->getTypeFromLastValidState();

	if (type == Token::COMMENT || type == Token::SPACE
			|| type == Token::NEWLINE) {
		reset(automat->getCharsBack());
		return nextToken();
	} else if (type == Token::INVALID) {
		reset(0);
		return nextToken();
	} else if (type == Token::INTEGER) {
		long int value = strtol(scanner_buffer, NULL, 10);
		reset(automat->getCharsBack());
		scanner_buffer[counter - automat->getCharsBack()] = '\0';
		if (errno == EINVAL) {
			cerr << "Error with converting String to long integer";
		} else if (errno == ERANGE) {
			cerr << "integer out of range";
		} else {
			return buildToken(automat->getLine(), automat->getColumn(),
					automat->getTypeFromLastValidState(), 0, value);
		}
		return nextToken();
	} else {
		scanner_buffer[counter-automat->getCharsBack()] = '\0';
		Information* information = buildInformation(scanner_buffer);
		reset(automat->getCharsBack());
		return buildToken(automat->getLine(), automat->getColumn(),
				automat->getTypeFromLastValidState(), information, 0);
		return token;
	}
}

void Scanner::reset(int badChars) {
	counter = 0;
	buffer->ungetchar(badChars);
	automat->getBack(badChars);
}

Token* Scanner::buildToken(int line, int column, Token::Type type,
		Information* information, long int value) {
	Token* token = new Token(type, line, column, information, value);
	return token;
}

Information* Scanner::buildInformation(char* lexem) {
	Information* information = new Information(lexem);
	return information;
}

